package com.example.finalproject.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Gun (
    @StringRes
    var stringResourceId: Int,
    @StringRes
    var priceResourceId: Int,
    @DrawableRes
    var imageResourceId: Int
)