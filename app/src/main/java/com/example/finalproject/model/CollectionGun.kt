package com.example.finalproject.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class CollectionGun (
    @StringRes val stringResourceId:Int,
    @DrawableRes val imageResourceId: Int
)