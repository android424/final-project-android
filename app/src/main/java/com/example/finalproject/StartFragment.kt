package com.example.finalproject

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.finalproject.adapter.ItemAdapter
import com.example.finalproject.data.Datasource
import com.example.finalproject.databinding.FragmentStartBinding

class StartFragment : Fragment() {
    private var binding: FragmentStartBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentStartBinding.inflate(inflater, container, false)
        fragmentBinding.recyclerView.layoutManager = LinearLayoutManager(activity)
        val myDataset = Datasource().loadCollection()
        var adapter = ItemAdapter(this, myDataset)
        fragmentBinding.recyclerView.adapter = adapter
        adapter.setOnItemClickListener(object : ItemAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
//                Toast.makeText(activity,"You click on item no. $position",Toast.LENGTH_SHORT).show()
                goToDetailPage()
            }

        })
        fragmentBinding.recyclerView.setHasFixedSize(true)
        return fragmentBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            startFragment = this@StartFragment
        }
    }

    fun goToDetailPage(){
        findNavController().navigate(R.id.action_startFragment_to_mainFragment)
    }

}