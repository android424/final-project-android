package com.example.finalproject.data

import com.example.finalproject.R
import com.example.finalproject.model.CollectionGun

class Datasource {
    fun loadCollection(): List<CollectionGun>{
        return listOf<CollectionGun>(
            CollectionGun(R.string.collection_1, R.drawable.set_prime1_0),
            CollectionGun(R.string.collection_2, R.drawable.set_prime2_0),
            CollectionGun(R.string.collection_3, R.drawable.set_reaver1_0),
            CollectionGun(R.string.collection_4, R.drawable.set_reaver2_0),
            CollectionGun(R.string.collection_5, R.drawable.set_ion1_0),
            CollectionGun(R.string.collection_6, R.drawable.set_ion2_0),
            CollectionGun(R.string.collection_7, R.drawable.set_rgx1_0),
            CollectionGun(R.string.collection_8, R.drawable.set_rgx2_0),
            CollectionGun(R.string.collection_9, R.drawable.set_chronoviod),
            CollectionGun(R.string.collection_10, R.drawable.set_prelude),
            CollectionGun(R.string.collection_11, R.drawable.set_runation),
            CollectionGun(R.string.collection_12, R.drawable.set_sentineloflight)
        )
    }
}