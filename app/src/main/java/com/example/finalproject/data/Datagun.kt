package com.example.finalproject.data

import com.example.finalproject.R
import com.example.finalproject.model.Gun

class Datagun {
    fun loadset1(): List<Gun>{
        return listOf<Gun>(
            Gun(R.string.classic_prime,R.string.price1775, R.drawable.prime_classic),
            Gun(R.string.spectre_prime,R.string.price1775, R.drawable.prime_spectre),
            Gun(R.string.guardian_prime,R.string.price1775, R.drawable.prime_gardian),
            Gun(R.string.vandal_prime,R.string.price1775, R.drawable.prime_vandal),
            Gun(R.string.axe_prime,R.string.price3550, R.drawable.prime_axe),
        )
    }
    fun loadset2(): List<Gun>{
        return listOf<Gun>(
            Gun(R.string.frenzy_prime,R.string.price1775, R.drawable.prime_frenzy),
            Gun(R.string.buckgy_prime,R.string.price1775, R.drawable.prime_buckgy),
            Gun(R.string.phantom_prime,R.string.price1775, R.drawable.prime_phantom),
            Gun(R.string.odin_prime,R.string.price1775, R.drawable.prime_odin),
            Gun(R.string.karambit_prime,R.string.price3550, R.drawable.prime_karambit),
        )
    }
}