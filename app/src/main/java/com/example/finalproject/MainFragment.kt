package com.example.finalproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.finalproject.adapter.GunAdapter
import com.example.finalproject.data.Datagun
import com.example.finalproject.databinding.FragmentMainBinding


class MainFragment : Fragment() {
    private var bunding: FragmentMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)
        fragmentMainBinding.recyclerViewGun.layoutManager = LinearLayoutManager(activity)
        val myDataset = Datagun().loadset1()
        var adapter = GunAdapter(this, myDataset)
        fragmentMainBinding.recyclerViewGun.adapter = adapter
        fragmentMainBinding.recyclerViewGun.setHasFixedSize(true)
        return fragmentMainBinding.root
    }
}